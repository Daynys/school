
;(function(window, arg ){
  'use strict';
    //====== constructor =============

  function Factory( elm ) {
    //this.elm = elm; // #scrollbar
    this._init(elm); //

    this._checkViewport();
    this._initEvents();
  }

  Factory.prototype._init = function(elm){
    this.scrollbar = elm;
    this.maxScrollHeight = 0;
    this.scrollVectorValue = 4;
  }; // _init

  Factory.prototype._initEvents = function(){
    var self = this;

    // ##
    window.addEventListener('resize', function(){ self._checkViewport(); });
    window.addEventListener('wheel', function(tmp){ self._checkScroll(tmp); } );


  }; // _initEvents

  Factory.prototype._checkViewport = function(){

    var self = this;
    var tBody = document.body
        , tHtml = document.documentElement
        , tHeight ;

    if (typeof document.height !== 'undefined')
      tHeight = document.height // For webkit browsers
    else
      tHeight = Math.max( tBody.scrollHeight, tBody.offsetHeight, tHtml.clientHeight, tHtml.scrollHeight, tHtml.offsetHeight );

    this.maxScrollHeight = tHeight;
    self._checkScroll();

      // var tmp = {}
      //    , docElm = window.document.documentElement;
      // tmp['ViewPortWidth'] = docElm['clientWidth'] > window['innerWidth'] ?
      //          tmp['clientWidth'] : window['innerWidth'] ;
      // console.log( tmp['ViewPortWidth'] ); // get w on vp

  } // _checkViewport

  Factory.prototype._checkScroll = function(obj){

    console.log('scroll' );
    var self = this;
    if( obj !== undefined ){
      window.scrollBy(0, obj.deltaY / self.scrollVectorValue );
      // return ;
    }

    if( innerHeight >= self.maxScrollHeight)
      this.scrollbar.style.height = "0px";
    else {
      this.scrollbar.style.height = "" + innerHeight*innerHeight/self.maxScrollHeight + "px";
      //scrollbar.style.top = "" +  innerHeight*window.pageYOffset/maxScrollHeight+ "px";
      this.scrollbar.style.transform = "translate3d(0px, "+  innerHeight*window.pageYOffset/self.maxScrollHeight +"px, 0px)";
    } // else

  }; // _checkScroll

  //return Factory;
  window.CstmScrollbar = Factory;
})(window );
